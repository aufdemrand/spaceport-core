package spaceport.library.manual

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@Retention(RetentionPolicy.RUNTIME)
@interface Explain {

    String contents() default ""
    String title() default "New unnamed explaination"
    String group() default "Unnamed"
    int    weight() default 1

}