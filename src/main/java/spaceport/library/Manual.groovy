package spaceport.library

import org.apache.commons.collections.bag.HashBag
import spaceport.bridge.Command
import spaceport.computer.alerts.Alert
import spaceport.computer.alerts.Result
import spaceport.computer.alerts.results.HttpResult
import spaceport.computer.utils.ClassScanner
import spaceport.library.manual.Documentation
import spaceport.library.manual.Explain

import java.lang.annotation.Annotation
import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.lang.reflect.Method

@Explain(
        group = 'Spaceport Manual',
        title = 'Overview',
        weight = 100,
        contents = $/<p>
        The Spaceport Manual contains documentation, examples, and guides for all internal systems.
        It's purpose is to equip advanced users and parts developers everything needed to work
        with the Spaceport. The Manual also allows for additional parts to be documented as well allowing
        for a common way to document internal systems as well as external parts.
        </p>
        /$)
class Manual {

    static List<Documentation> docs = []

    @Explain(
            group = 'Spaceport Manual',
            title = 'Accessing the Manual',
            weight = 200,
            contents = $/<p>
            Accessing the Manual is achieved by sending a http request to the Spaceport's public
            address specifying /manual/ as the path. This contains a dynamically built HTML page
            for viewing the contents of the Manual.</p>

            <p>
            The Manual Documentation can also be accessed progmatically by accessing
            <em>spaceport.library.Manual.docs</em> which is reference to a <em>java.util.List</em> of
            <em>spaceport.library.manual.Documentation</em> objects, here out referred to as simply
            <em>Documentation Objects</em>. <em>Documentation Objects</em> contains a <em>group, title, weight, contents</em> and <em>reference</em>.
            Documentation contents is built from <em>@Explain annotations</em> inside the source code using the
            parameters specified. In addition, the <em>reference</em> contains the <em>java.lang.Class</em> that contains the
            annotation.
            </p>
        /$)
    @Alert("on /manual/ hit")
    public static homepage(HttpResult r) {

        def html = $/
            <html>
            <head>
                <title>Spaceport Manual</title>
                <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet' type='text/css'>
                <style>
                    body {
                        font-family: 'Roboto Slab';
                        padding: 20px;
                    }
                    flex-list {
                        border: 1px solid rgba(0, 0, 0, 0.23);
                        padding: 20px;
                        margin-top: 30px;
                        margin-bottom: 40px;
                        display: block;
                        background-color: lightgoldenrodyellow;
                    }
                    flex-grid {
                        border: 1px solid lightgray;
                        background-color: whitesmoke;
                        border-left: 3px solid salmon;
                        display: block;
                        padding: 25px;
                    }
                    flex-cell[for=title] {
                        font-size: 22px;
                        padding-right: 20px;
                    }
                    flex-cell[for=group] {
                        color: gray;
                    }
                    flex-cell[for=contents] {
                        display: block;
                    }
                    h1 {
                        margin-bottom: 40px;
                    }
                </style>
            </head>
            <body>
                <h1>Spaceport Manual</h1>

                <flex-list>
            /$

        // Groups
        def groups = getGroups()
        for (group in groups.uniqueSet())
            html += $/
                    <flex-item>${group} </em>${groups.getCount(group)} entries</em></flex-item>
            /$

        html += '</flex-list>'

        // Docs
        for (doc in docs)
            html += $/
                <flex-grid for='explaination'>
                    <flex-cell full for='title'>${doc.title}</flex-cell>
                    <flex-cell half for='group' lavel='Group:'>${doc.group}</flex-cell>
                    <flex-cell full for='contents'>${doc.contents}</flex-cell>
                </flex-grid>
            /$

        r.response.writer.println(html += '</body></html>')

    }


    private static getGroups() {
        def groups = new HashBag()
        for (doc in docs)
            groups.add(doc.group)
        return groups;
    }


    @Explain(
            group = 'Systems Manual',
            title = 'Adding content to the Manual',
            weight = 300,
            contents = $/<p>
            The Manual is built dynamically upon the <em>'on initialize' Alert</em>. This process will
            scan all loaded classes for the <em>@Explain Annotation</em>. To add a <em>Documentation Object</em> to
            the manual, annotate any class, field, property, or method. See below as an example.
            </p>

            <code>
            // First, select a relative class, field, property or method to annotate. The actual ¬
            // insertion point isn't relative, except that the annotation content itself should  ¬
            // be relevant to the code annotated. ¬
            ¬
            // Next, include the annotation named '@Explain' and specify the content. ¬
            @Explain( ¬
            ····// The 'group' is the section being referenced. This group will be available in ¬
            ····// the table of contents. ¬
            ····group = 'myClass Reference', ¬
            ····// The 'title' is typically a couple of words or a short sentence describing ¬
            ····// the section being referenced. ¬
            ····title = 'myClass Overview', ¬
            ····// The 'weight' will control in the order in which the reference entry shows ¬
            ····// in the systems manual group. A value of '1' will ensure the entry is first ¬
            ····// in the group specified. ¬
            ····weight = 1, ¬
            ····// And finally the contents is the meat of the entry and should contain the ¬
            ····// relevent information to be documented. Use simple HTML elements such as ¬
            ····// &lt;p&gt;, &lt;strong&gt;, &lt;em&gt;, and &lt;br&gt; to format the contents. ¬
            ····contents = ''' ¬
            ········&lt;p&gt; ¬
            ········MyClass is an important part of your Spaceport. ¬
            ········&lt;/p&gt; ¬
            ····''') ¬
            class MyClass { ¬
            ¬
            ····// Now, a more concise example.  ¬
            ¬
            }
            </code>/$)

    @Alert("on initialize")
    public static initialize(Result r) {
        Command.report([
                'group' : 'Documentation',
                'message' : 'Initializing library.' ])
        // println 'Initializing Documentation.'
        docs.clear();

        def classes = ClassScanner.getLoadedClasses()

        // Build docs by reading annotations
        for (Class c in classes) {

            def explainations = []

            // First for the class
            for (Annotation a in c.getAnnotations()) {
                // println(a.class)
                if (a.annotationType().equals(Explain.class)) explainations.add(a)
            }
            // And also on all the methods, constructors, and fields contained
            for (Method m in c.getMethods())
                for (Annotation a in m.getAnnotations()) {
                    // println(a.class)
                    if (a.annotationType().equals(Explain.class)) explainations.add(a)
                }
            for (Constructor m in c.getConstructors())
                for (Annotation a in m.getAnnotations()) {
                    // println(a.class)
                    if (a.annotationType().equals(Explain.class)) explainations.add(a)
                }
            for (Field m in c.getFields())
                for (Annotation a in m.getAnnotations()) {
                    // println(a.class)
                    if (a.annotationType().equals(Explain.class)) explainations.add(a)
                }

            // println('explainations ' + explainations)

            explainations.each {
                docs.add(new Documentation(
                        reference: c.getSimpleName(),
                        group: it.group(),
                        contents: it.contents().replace('·','&nbsp;').replace('¬','<br>'),
                        title: it.title(),
                        weight: it.weight()))
            }

            Command.report([
                    'group' : 'Documentation',
                    'message' : docs.size().toString() + ' library entries have been loaded.',
                    'documents' : docs ])
        }

    }




}
