
package spaceport.computer.alerts

import spaceport.computer.Alerts

/**
 * Created by Jeremy on 6/4/2015.
 */

abstract class AlertType {

    public def hookString;

    public abstract void buzz(def context);

    public AlertType(def hookString) {
        // Remember the hookString
        this.hookString = hookString
        // Notify Hooks of this Hook
        Alerts.notifyOf(this)
    }

    public abstract void dispose();

}
