package spaceport.computer.alerts

import spaceport.bridge.Command
import spaceport.computer.utils.ClassScanner

import java.lang.reflect.Method
import java.lang.reflect.Modifier

/**
 * Created by Jeremy on 6/9/2015.
 */
class AnnotatedAlert extends AlertType {


    // Hooks referenced in Hooks.class are WeakReferenced to avoid unnecessary memory
    public static staticHooks = []


    public static getStaticHooks() {

        Command.report([
                'group' : 'Hooks',
                'message' : 'Disposing of annotated hooks.' ])

        // Rid ourselves of old hooks -- BE GONE!
        for (AnnotatedAlert h in staticHooks) {
            // Stop execution while 'garbage collecting' is being called
            h.dispose();
        }

        staticHooks.clear();


        // Iterate through loaded classes for Hook annotations
        for (Class c in ClassScanner.getLoadedClasses())
            for (Method m in c.getMethods())
                if (m.isAnnotationPresent(Alert.class))
                    // Must be static
                    if (Modifier.isStatic(m.getModifiers())) {
                        // It is! New hook time -- use value of annotation for the 'hook name'
                        AnnotatedAlert annotatedHook = new AnnotatedAlert(m.getAnnotation(Alert.class).value(), c.getName(), m.getName())
                        annotatedHook.m = m
                        staticHooks.add(annotatedHook)
                        // println 'Hooking "' + c.getName() + '->' + m.getAnnotation(Hook.class).value() + '"'
                    }

        Command.report([
                'group' : 'Hooks',
                'message' : 'Finished hooking annotated methods.',
                'bridge' : staticHooks ])
    }

    //
    // Instanced
    //

    def className, methodName;

    public AnnotatedAlert(def hookString, def className, def methodName) {
        super(hookString)
        this.className = className;
        this.methodName = methodName;
    }

    // This is our method to run when the hook is called
    Method m;

    @Override
    void buzz(def context) {
        if (m != null)
            m.invoke(null, context);
    }

    // Dispose of method when hooks are 'reloaded'
    void dispose() {
        this.m = null;
    }

}
