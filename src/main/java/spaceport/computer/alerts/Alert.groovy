package spaceport.computer.alerts

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


// Hook for AnnotatedHooks, the main interface for dealing with hooks
// with server-side Groovy.

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) //can use in method only.
public @interface Alert {

    String value();

}