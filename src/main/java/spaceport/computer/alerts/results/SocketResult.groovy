package spaceport.computer.alerts.results

import groovy.json.JsonBuilder
import org.eclipse.jetty.websocket.api.Session
import spaceport.communications.socket.SocketHandler
import spaceport.computer.alerts.Result
import spaceport.computer.memory.physical.Document
import spaceport.personnel.Client

/**
 * Created by Jeremy on 5/31/2015.

 'handler'     : this,
 'session'     : session,
 'time'        : System.currentTimeMillis(),
 'args'        : json,
 'handler-id'  : json['handler-id'],
 'flags'       : flags,

 */
class SocketResult extends Result {

    boolean cancelled = false

    def context = [:]

    public SocketHandler getHandler() {
        return context['handler'] as SocketHandler
    }

    public Session getSession() {
        return context['session'] as Session
    }

    public ConfigObject getFlags() {
        return context['flags'] as ConfigObject
    }

    public Long getTime() {
        return context['time'] as Long
    }

    public def getArg(String arg) {
        if (context['args'][arg] instanceof Map ||
            context['args'][arg] instanceof Collection ||
            context['args'][arg] instanceof Number )
            return context['args'][arg]
        else
            return context['args'][arg] as String
    }

    SocketResult(Object context) {
        super(context)
    }

    void writeToRemote(String string) {
        getSession().getRemote().sendString(string)
    }

    void writeToRemote(Map map) {
        // println ('Sending via socket -> ' + map)
        getSession().getRemote().sendString(new JsonBuilder(map).toPrettyString())
    }

    public Client getClient() {
        return Client.getClient(handler.flags.get('user-attached') as String)
    }

    public Document getClientDoc() {
        return Client.getClient(handler.flags.get('user-attached') as String).userDocument
    }

}
