package spaceport.computer.alerts.results

import spaceport.computer.alerts.results.SocketResult
import spaceport.computer.memory.physical.Document
import spaceport.personnel.Client

/**
 * Created by denizen_ on 11/28/15.
 */
class ConsoleResult extends SocketResult {

    public html
    def returnOptions = [:]

    ConsoleResult(Object context) {
        super(context)
    }

    public setOutput(String html) {
        this.html = html
    }

    public Map getOptions() {
        return getArg('options') as Map
    }

    public def setOption(String option, def value) {
        returnOptions.put(option, value)
    }

}
