package spaceport.computer.memory.physical

import org.codehaus.jackson.map.ObjectMapper
import spaceport.Spaceport

class ViewDocument {

    //
    // Static
    //

    public static ViewDocument get(def id, def database) {
        ViewDocument doc = Spaceport.physical_memory_core.getDoc('_design/' + id, database, ViewDocument.class)
        if (doc == null) {
            Spaceport.physical_memory_core.addDoc('_design/' + id, database);
            doc = Spaceport.physical_memory_core.getDoc('_design/' + id, database, ViewDocument.class)
        }
        if (doc != null) doc.database = database
        return doc
    }

    public static ViewDocument getIfExists(def id, def database) {
        ViewDocument doc = (ViewDocument) Spaceport.physical_memory_core.getDoc('_design/' + id, database, ViewDocument.class)
        if (doc != null) doc.database = database
        return doc
    }

    public static boolean exists(def id, def database) {
        return Spaceport.physical_memory_core.getDoc(id, database, ViewDocument.class) != null
    }


    //
    // Instance
    //

    transient public def database;

    public View getView(def id) {
        return View.get(_id, id, database)
    }

    public setView(def id, def map) {
        setView(id, map, null);
    }

    public setView(def id, def map, def reduce) {
        if (!views.containsValue(id))
            views[id] = [:]
        def view = views[id]

        view['map'] = map;
        if (reduce != null)
            views['reduce'] = reduce
        else views.remove('reduce')
    }

    public boolean hasView(def id) {
        return views.containsValue(id)
    }

    // Saves any changes to the Database
    public Operation save()   {
        // Result r = Hooks.invoke('on document save', [ 'type' : type ])
        return Spaceport.physical_memory_core.updateDoc(this, database)
    }

    // Removes this record from the Database
    public Operation remove() { return Spaceport.physical_memory_core.removeDoc(this, database) }


    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this)
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //
    // Serialized
    //

    // View Documents have an ID and revision (managed by CouchDB)
    def _id, _rev

    def language = 'javascript'

    def views = [:]

}
