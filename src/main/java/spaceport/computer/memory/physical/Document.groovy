package spaceport.computer.memory.physical

import com.google.common.collect.Maps
import de.danielbechler.diff.ObjectDifferBuilder
import de.danielbechler.diff.node.DiffNode
import org.apache.commons.fileupload.FileItem
import org.apache.commons.fileupload.FileItemFactory
import org.apache.commons.fileupload.FileUploadException
import org.apache.commons.fileupload.disk.DiskFileItemFactory
import org.apache.commons.fileupload.servlet.ServletFileUpload
import org.apache.commons.io.FilenameUtils
import org.codehaus.jackson.map.ObjectMapper
import spaceport.Spaceport
import spaceport.computer.Alerts
import spaceport.computer.alerts.Result

import javax.servlet.http.HttpServletRequest
import javax.xml.bind.DatatypeConverter
import java.nio.file.Files


class Document {


    //
    // Static
    //


    public static Document get(def id, def database) {
        def core = Spaceport.physical_memory_core
        Document doc = core.getDoc(id, database, Document.class)
        if (doc == null) {
            core.addDoc(id, database);
            doc = core.getDoc(id, database, Document.class)
        }
        if (doc != null) doc.database = database
        // println ('Getting document ' + doc)
        return doc
    }


    public static Document getNew(def database) {
        return get(UUID.randomUUID().toString().replace('-',''), database)
    }


    public static Document getIfExists(def id, def database) {
        def core = Spaceport.physical_memory_core
        Document doc = (Document) core.getDoc(id, database, Document.class)
        if (doc != null) doc.database = database
        // May return null
        return doc
    }


    public static boolean exists(def id, def database) {
        return Spaceport.physical_memory_core.getDoc(id, database, Document.class) != null
    }


    //
    // Instance
    //


    transient public String database;


    public getAttachment(String attachment, OutputStream o) {
        return Spaceport.physical_memory_core.getDocAttachment(attachment, _id, database, o)
    }


    @Deprecated
    public void addAttachments(HttpServletRequest request) {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) {
            // println('multipart' + request)
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                List uploads = upload.parseRequest(request);
                for(FileItem i in uploads) {
                    try {
                        if (i.getName() == null)
                            continue;
                        // println(i.getFieldName() + ' -> ' + i.getName())
                        def data =  DatatypeConverter.printBase64Binary(i.get())
                        def name = FilenameUtils.getName(i.getName())
                        def type = i.getContentType() != null ? i.getContentType() : 'application/download';
                        _attachments[name] = ['content_type': type, 'data': data]
                        // Add time uploaded to dates[:]
                        dates[name] = System.currentTimeMillis()
                    } catch (Exception e) { println(i); e.printStackTrace() }
                }
                print(_attachments)
            } catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    // Stores away a file as a Base64 attachment -- and notes details
    public String addAttachment(File file, Map details) {

        // Give the file a unique ID for storing away
        def uuid = 'file-' + UUID.randomUUID().toString();
        try {
            // Convert the file to base64 -- this is how CouchDB stores files
            def data = DatatypeConverter.printBase64Binary(file.bytes)
            // Get the file name
            def name = FilenameUtils.getName(file.getName())
            // Get the file mime-type
            def type = Files.probeContentType(file.toPath())

            // Add some of this to the details -- these are stored alongisde the file in the database
            details['name'] = name
            details['type'] = type
            details['upload-time'] = System.currentTimeMillis()

            // Now put all this in the database
            _attachments[uuid] = ['content_type' : type, 'data' : data]
            // File info is stored along side, for reference
            if (private_fields['attachments'] == null) private_fields['attachments'] = [:]
            (private_fields['attachments'] as Map).put(uuid, details)

        } catch (Exception e) { e.printStackTrace() }

        // Return the uuid of the file just uploaded
        return uuid;
    }


    // Saves any changes to the Database
    public Operation save() {
        // Get old version
        Document old = get(_id, database)
        DiffNode difference = ObjectDifferBuilder.buildDefault().compare(this, old)
        Alerts.invoke('on document save',  [ 'type' : type, 'old' : old, 'document' : this, 'difference' : difference ])
        Operation o = Spaceport.physical_memory_core.updateDoc(this, database)
        Alerts.invoke('on document saved', [ 'type' : type, 'old' : old, 'document' : this, 'difference' : difference ])
        return o;
    }


    // Removes this record from the Database
    public Operation remove() { return Spaceport.physical_memory_core.removeDoc(this, database) }


    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this)
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Gets the title of the record by looking in some familiar places
    String getTitle() {
        // First the title field, then private fields, then just use the _id
        if (fields.containsKey('title'))
            return fields['title']
        else if (private_fields.containsKey('title'))
            return private_fields['title']
        else if (type != null)
            return 'Document ' + _id
    }



    //
    // Serialized -- information stored/read to the database
    //

    // Documents have an ID and revision (managed by CouchDB)
    def _id, _rev

    // And inline-attachments -- stored in base64 format
    def _attachments = [:]

    // They have Maps, keyed by an identifier, of Strings and List<String>s, and can be manipulated
    // with HTTP requests easily. These fields should be considered 'exposed'
    def fields = [:]

    // Private fields, also keyed by an identified, can contain any Object, to be manipulated with groovy-scripts only
    def private_fields = [:]

    // Dates which contain a Map of Long in the format of 'currenttimemillis', keyed by an identifier
    def dates = [:]

    // Stats can contain Integers or Booleans in a Map keyed by an identifier
    def stats = [:]

    // ...and a type, for a universal identification location
    def type




}
