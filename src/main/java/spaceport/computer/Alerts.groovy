package spaceport.computer

import spaceport.bridge.Command
import spaceport.computer.alerts.AlertType
import spaceport.computer.alerts.Result

import java.lang.ref.WeakReference
import java.util.concurrent.CopyOnWriteArrayList


class Alerts {

    //
    // Static
    //

    private static def registered = new CopyOnWriteArrayList<WeakReference<AlertType>>()


    public static void notifyOf(AlertType hookHandler) {
        registered.add(new WeakReference<AlertType>(hookHandler))
    }


    public static Result invoke(String hookString, def context) {

        // println hookString

        // The result contains infos passed along to the hook, and accepts information
        // about the outcome (or, 'result') of the hook code.
        def result;

        // Check result-type -- some events have special result classes to make
        // things easier and faster when interacting with the API via scripting
        if (context['result-type'] instanceof Class) {
            // if (Result.class.isAssignableFrom(context['result-type'] as Class)) {
                result = (context['result-type'] as Class).getConstructor(Object.class).newInstance(context)
                // TODO: Not sure why newInstance isn't propogating context passed -- set manually for now
                result.context = context;
            // }
        }

        // -- but default to standard Result if none specified.
        if (result == null)
            result = new Result(context);

        // Buzz the Hook to check for execution
        for (WeakReference<AlertType> hook in registered) {
            if (hook.get() != null) {
                try {
                    if (hook.get().hookString.toLowerCase() == hookString.toLowerCase()) {
                        hook.get().buzz(result)
                        // Report the invoke only if there is a buzz.
                        if (result.report)
                            Command.report(['group'  : 'hooks',
                                            'message': 'Hook "' + hookString + '" has been buzzed.',
                                            'hook-id': hookString,
                                            'hook' : hook.get(),
                                            'result': result,
                                            'context': context])
                    }
                } catch (Exception e) {
                    e.printStackTrace()
                    // Report if the buzz failed.
                    Command.report(['group'  : 'hooks',
                                    'message': 'Hook "' + hookString + '" has been buzzed, but failed!',
                                    'hook-id': hookString,
                                    'exception' : e,
                                    'context': context])
                }
            }
        }

        // Return mutated result
        return result;
    }




    //
    // Deprecated
    //


    @Deprecated
    private static Map<String, Map<String, Closure>> hooks = new HashMap<String, Map<String, Closure>>();


    @Deprecated
    public static boolean isHooked(String id) {
        for (Map<String, Closure> hks in hooks.values()) {
            for( Iterator<Map.Entry<String, Closure>> it = hks.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<String, Closure> entry = it.next()
                if(entry.getKey().equals(id)) {
                    // found the id, return true -- this id is already hooked
                    return true;
                }
            }
        }
        // nothing found?
        return false;
    }


    @Deprecated
    public static void add(String hook, String id, Closure closure) {
        if (hook == null || id == null || closure == null)
            return;

        if (hooks[hook.toLowerCase()] == null)
            hooks[hook.toLowerCase()] = new HashMap<String, Closure>();

        // Add hook to the list
        hooks[hook.toLowerCase()][id.toLowerCase()] = closure;

        Command.report([ 'group' : 'hooks',
                         'message' : 'A new hook added.',
                         'hook' : hook,
                         'id' : id ])

    }


    @Deprecated
    public static void remove(String id) {
        for (Map<String, Closure> hks in hooks.values()) {
            for( Iterator<Map.Entry<String, Closure>> it = hks.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<String, Closure> entry = it.next()
                if(entry.getKey().equals(id)) {

                    Command.report([ 'group' : 'hooks',
                                     'message' : 'A hook has been removed.',
                                     'id' : id ])

                    it.remove()
                }
            }
        }
    }


}
