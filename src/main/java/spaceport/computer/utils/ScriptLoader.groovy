package spaceport.computer.utils

import spaceport.Spaceport
import spaceport.communications.utils.GrooscriptCompiler

/**
 * Created by Jeremy on 10/27/2015.
 */
class ScriptLoader {

    // Uses 'source_path' for fetching sources

    public static String getSource(String path) {
        // If its groovy compatible with GrooScript, convert it to .js
        if (path.endsWith('.groovy')) {
            return GrooscriptCompiler.convert(new File(Spaceport.source_path + path).getText())
         }
        // Otherwise, assume ready for output
        else {

            return new File(Spaceport.source_path + path).getText()
        }
    }


}
