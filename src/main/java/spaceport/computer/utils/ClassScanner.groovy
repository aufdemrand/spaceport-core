package spaceport.computer.utils

import com.google.common.reflect.ClassPath

/**
 * Created by denizen_ on 10/19/15.
 */
class ClassScanner {


    public static def base_package = 'spaceport'

    public static classes = [];

    public static GroovyClassLoader classLoader

    public static List<Class> getLoadedClasses() {
        return classes;
    }

    public static initialize() {
        classes.addAll(loadedClasses)
        def loader = Thread.currentThread().getContextClassLoader();
        classes.addAll(ClassPath.from(loader).getTopLevelClassesRecursive(base_package))
        return classes;
    }






}
