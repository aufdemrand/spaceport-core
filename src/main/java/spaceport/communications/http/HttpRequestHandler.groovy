package spaceport.communications.http

import spaceport.computer.Alerts
import org.eclipse.jetty.server.Request
import org.eclipse.jetty.server.handler.AbstractHandler
import spaceport.computer.alerts.results.HttpResult
import spaceport.personnel.ClientRegistry

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by Jeremy on 1/15/2015.
 */
class HttpRequestHandler extends AbstractHandler {


    public void handle(String target,
                       Request baseRequest,
                       HttpServletRequest request,
                       HttpServletResponse response)

            throws IOException, ServletException {

        // For now, just handle _everything_ as OK text/HTML, unless otherwise set.
        baseRequest.setHandled(true);

        // Have to catch all errors here or else Jetty seems to catch and hide
        // them itself.

        try {

            // Disable page caching by default
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);

            // And allow cross-domain
            response.addHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
            response.addHeader("Access-Control-Allow-Credentials", "true");
            response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
            response.addHeader("Access-Control-Allow-Headers", "Content-Type");

            // OPTIONS? Return with a '200' response -- some browsers send this before an actual
            // request to get header contents.
            if (request.getMethod() == "OPTIONS") {
                response.setStatus(200)
                return;
            }

            // Define some context and send an Alert
            def context = [
                    // intended to be immutable
                    'request'  : request,
                    'response' : response,
                    'personnel'   : ClientRegistry.getClientBySessionId(request.getSession().getId()),
                    'hit-time' : System.currentTimeMillis(),
                    // intended to be mutable
                    'content-type' : "text/html;charset=utf-8",
                    'status'   : 200,
                    // Specify subclass of result type for access to helper methods based on the type
                    'result-type': HttpResult
            ]

            Alerts.invoke('on page hit', context)
            Alerts.invoke('on ' + target + ' hit', context)
            Alerts.invoke('on ' + target + ' ' + request.getMethod(), context)

            // Use the final mutation of the Result to set status (default 200), and content-type
            // Anything more complex should alter the response itself, for example -- Headers/etc.
            response.setStatus(context['status'] as Integer)
            response.setContentType(context['content-type'] as String)

        } catch (Exception e) {
            // 501 tells the client there was a serious issue!
            e.printStackTrace();
            response.setStatus(501);
        }

    }


}
