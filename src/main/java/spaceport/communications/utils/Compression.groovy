package spaceport.communications.utils

import java.util.zip.GZIPOutputStream


// Provides compression utilities to help send data more efficiently

class Compression {

    public static String gzip(String string) {
        // Only compress if there's something to compress
        if (string == null || string.length() == 0)
            return string;
        // GZIP data, send along zipped string
        ByteArrayOutputStream out = new ByteArrayOutputStream()
        GZIPOutputStream gzip = new GZIPOutputStream(out)
        gzip.write(string.getBytes())
        gzip.close()
        return out.toString("UTF-8")
    }

}
