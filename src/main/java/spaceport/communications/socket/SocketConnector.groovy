package spaceport.communications.socket

import org.eclipse.jetty.websocket.server.WebSocketHandler
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory

public class SocketConnector extends WebSocketHandler
{
    @Override
    public void configure(WebSocketServletFactory factory)
    {
        factory.getPolicy().setIdleTimeout(60000)
        factory.register(SocketHandler.class)
    }



}