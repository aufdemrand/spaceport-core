package spaceport.communications.socket

import groovy.json.JsonSlurper
import spaceport.computer.Alerts
import spaceport.computer.alerts.results.SocketResult
import org.eclipse.jetty.websocket.api.Session
import org.eclipse.jetty.websocket.api.WebSocketAdapter
import spaceport.computer.alerts.Result
import spaceport.personnel.ClientRegistry

public class SocketHandler extends WebSocketAdapter {

    def flags = new ConfigObject()

    @Override
    public void onWebSocketConnect(Session sess) {

        super.onWebSocketConnect(sess);

        println 'New socket.'

        def context = [
                'handler' : this,
                'session' : session,
                'time'    : System.currentTimeMillis(),
                'flags'   : flags
        ]

        // Allow socket connections (intial contact) to be monitored/refused
        Result o = Alerts.invoke('on socket connect', context)
        if (o.cancelled) {
            sess.close()
        };

    }

    @Override
    public void onWebSocketBinary(byte[] payload, int offset, int len) {

        println('WebsocketBinary incoming! ' + payload.size())

    }


    @Override
    public void onWebSocketText(String message) {

        // 'message' will be dealt with via a JSON structure
        def json = new JsonSlurper().parseText(message)

        // Must have a 'handler-id' of the socket handler
        if (json['handler-id'] == null) return;

        def context = [
                'handler'     : this,
                'session'     : session,
                'time'        : System.currentTimeMillis(),
                'args'        : json,
                'handler-id'  : json['handler-id'],
                'flags'       : flags,
                // specify result type
                'result-type' : SocketResult
        ]

        Result o = Alerts.invoke('on socket data', context)
        if (o.cancelled) return;

        Alerts.invoke('on socket ' + json['handler-id'], context)
    }


    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        super.onWebSocketClose(statusCode,reason);

        // invoke hook for socket closed
        def context = [
                'handler'    : this,
                'session'    : session,
                'time'       : System.currentTimeMillis(),
                'flags'      : flags
        ]

        Alerts.invoke('on socket closed', context)

        // Unregister socket from any authenticated clients
        ClientRegistry.removeSocket(this);
    }


    @Override
    public void onWebSocketError(Throwable cause) {
        super.onWebSocketError(cause);
        // cause.printStackTrace(System.err);
    }
}