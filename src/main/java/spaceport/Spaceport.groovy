package spaceport

import org.yaml.snakeyaml.Yaml
import spaceport.computer.memory.physical.CouchHandler
import spaceport.computer.utils.ClassScanner
import spaceport.engineering.PartsStore
import spaceport.communications.CommunicationsArray


class Spaceport {

    static CouchHandler physical_memory_core;

    static def include_paths = []
    static def static_path   = [:]
    static def source_path   = new String()


    public static void main(String[] args) throws Exception {

        // Read the specified config file
        if (args.length == 0)
            throw new Exception('Must specify the path of a port-config.yml.')

        def path = args[0]
        def config = []

        Yaml yaml = new Yaml();

        try {
            InputStream stream = new FileInputStream(new File(path));
            // Parse the YAML file and return the output as a series of Maps and Lists
            config = yaml.load(stream) as Map<String,Object>;
        } catch (Exception e) {
            throw new Exception('Failed to read port-config.')
        }

        // Initialize the Database
        physical_memory_core = new CouchHandler(config.'physical memory core'.'address')
        include_paths = config.'modules'.'include paths'
        static_path   = config.'static'.'root path'
        source_path   = config.'source'.'root path'

        // Initialize any Groovy parts
        ClassScanner.initialize()
        PartsStore  .initialize()

        // Start the Communications Array (webserver)
        new Thread(
                new CommunicationsArray(config.'host'.'port' as Integer, config.'host'.'address' as String))
                .start()

    }

}
