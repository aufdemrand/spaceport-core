package spaceport.personnel

import groovy.json.JsonBuilder
import org.eclipse.jetty.websocket.api.Session
import spaceport.communications.socket.SocketHandler
import spaceport.computer.memory.physical.Document

import java.lang.ref.WeakReference

class Client {

    public static Client getClient(String user_id) {
        // Search active clients first
        for (Client client in ClientRegistry.activeClients) {
            if (client.user_id == user_id)
                return client;
        }
        // Else, make a new client
        Client client = new Client()
        client.user_id = user_id;
        ClientRegistry.registerClient(client)
        return client;
    }

    ConfigObject flags = new ConfigObject();

    // Unique identifier between the remote client (user on the other end)
    // and the server client (this), handled server-side by HashSessionManager (Jetty)
    String session_id

    // The socket used to communicate with the user on the other end, if connected.
    Set<WeakReference<SocketHandler>> sockets = new HashSet<WeakReference<SocketHandler>>()

    // The _id of the User database record
    String user_id

    void attachSocketHandler(SocketHandler client) {
        sockets.add(new WeakReference<SocketHandler>(client));
    }

    // For writing to all the socket clients at once
    void writeToClient(String string) {
        for (def socket in sockets)
            socket.get()?.getSession()?.getRemote()?.sendStringByFuture(string)
    }

    void writeToClient(Map map) {
        for (def socket in sockets)
            socket.get()?.getSession()?.getRemote()?.sendStringByFuture(new JsonBuilder(map).toPrettyString())
    }

    void writeToSession(Map map, Session session) {
        session?.getRemote()?.sendStringByFuture(new JsonBuilder(map).toPrettyString())
    }

    Document getUserDocument() {
         return Document.get(user_id, 'users');
    }


}
