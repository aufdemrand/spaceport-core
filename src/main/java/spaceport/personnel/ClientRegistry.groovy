package spaceport.personnel

import groovy.json.JsonBuilder
import spaceport.Spaceport
import spaceport.communications.socket.SocketHandler
import spaceport.computer.alerts.Alert
import spaceport.computer.alerts.Result
import spaceport.computer.alerts.results.HttpResult
import spaceport.computer.memory.physical.ViewDocument
import spaceport.library.manual.Explain

/**
 * Created by Jeremy on 6/15/2015.
 */
class ClientRegistry {


    @Explain(
            group = 'Personnel',
            title = 'Basic Personnel Database',
            weight = 200,
            contents = $/<p>
            Users registered as basic personnel can authenticate with a two-step pin authentication. This keeps the login procedure
            secure, yet quick.</p>
        /$)
    @Alert("on include initialize")
    public static registerViews(Result r) {

        // Check users database, create if necessary
        if (!Spaceport.physical_memory_core.containsDatabase('users')) {
            Spaceport.physical_memory_core.createDatabase('users')
        }

        // Register view for authentication check
        ViewDocument views = ViewDocument.get('user-views', 'users')

        // The view is javascript that interacts with CouchDB
        // The following function is run for each document in the database and the information
        // emitted are indexed for fast retrieval
        views.setView('authenticate', '''
            function (doc) {
                emit(doc.fields['user-name'], doc.private_fields['key-code'] || 'undefined');
            }
            ''')

        views.save();
    }


    // Active clients have ongoing connections
    static private activeClients = []


    static boolean isActive(String sessionId) {
        // Internal
        for (Client client in activeClients) {
            if (client.session_id == sessionId)
                return true
        }
        return false
    }


    // To be called by some kind of 'login' code, probably via a Console
    static void registerClient(Client c) {
        // Internal
        activeClients.add(c)
    }

    // Called when a socket is disconnected
    static void removeSocket(SocketHandler socketHandler) {
        for (Client client in activeClients) {
            if (client.sockets.contains(socketHandler))
                client.sockets.remove(socketHandler);
        }
    }


    // Gets a client based on their session_id (http request)
    static Client getClientBySocket(SocketHandler socketHandler) {
        for (Client client in activeClients) {
            if (client.sockets.contains(socketHandler))
                return client
        }
        // None found? Create a new Client.
        def client = new Client()
        registerClient(client)
        return client
    }



    // Gets a client based on their session_id (http request)
    static Client getClientBySessionId(String sessionId) {
        for (Client client in activeClients) {
            if (client.session_id == sessionId)
            return client
        }
        // None found? Create a new Client.
        def client = new Client()
        client.session_id = sessionId
        registerClient(client)
        return client
    }


}
