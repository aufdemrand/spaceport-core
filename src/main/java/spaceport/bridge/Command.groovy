package spaceport.bridge

import groovy.json.JsonBuilder
import spaceport.communications.socket.SocketHandler
import spaceport.computer.Alerts
import spaceport.computer.alerts.Alert
import spaceport.computer.alerts.Result
import spaceport.computer.alerts.results.SocketResult

/**
 * Created by Jeremy on 5/31/2015.
 */
class Command {

    static def clients = [:]

    static report(def report) {
        // Only report if there are console clients connected
        if (clients.size() > 0)
        try {
            def reported = [:]
            // handle simple string report but allow for
            // more complex report
            if (report instanceof String) {
                reported['value'] = ['message' : report]
            } else {
                // Must 'dumb down' the report to avoid cyclical references when
                // converting to JSON -- the following will produce
                // StackOverflowErrors quite often!
                // reported['value'] = new JsonBuilder(report).toString()
                reported['value'] = mapToSimpleJsonFriendly(report)
            };
            // send along report timestamp
            reported['time'] = System.currentTimeMillis()
            // tell the console client the intent of the socket data
            reported['intent'] = 'add-entry'
            // send to console clients
            for (SocketHandler c in clients.keySet()) {
                // Send by Future to avoid hanging the Thread
                try {
                    c.getRemote().sendStringByFuture(new JsonBuilder(reported).toString())
                } catch (NullPointerException e) { /* Catch this, the client is now gone... */ }
            }
        } catch (Exception e) { e.printStackTrace(); }
    }

    // Internal method for report(...)/etc. sending a 'good enough' json representation of
    // the object being reported/transmitted/etc. -- Not good for serialization/deserialization,
    // but it's good enough for debugging/logging without dealing with cyclical references
    // producing StackOverflowErrors.
    private static mapToSimpleJsonFriendly(Object object) {
        def conversion
        // If the object is a map, iterate through and convert each item toString()
        if (object instanceof Map) {
            conversion = [:]
            for (def objset in object.entrySet()) {
                conversion[objset.key.toString()] = mapToSimpleJsonFriendly(objset.value);
            }
        }
        // Not a map? Just convert the object toString()
        else conversion = object.toString();

        // Return the result -- caller must accept String or Map
        return conversion;
    }


    // Incoming data from the Client!
    // Use 'hook' system to delegate the packet received.
    @Alert('on socket console')
    public static getPacket(SocketResult r) {
        // Don't report this hook, since we're just handing it off to something
        // more specific
        print 'console'
        r.report = false;
        // Call Hook to handle the intent
        Alerts.invoke('console ' + r.getArg('intent'), r.context);
    }


    // Client has closed the socket by themselves!
    @Alert('on socket closed')
    public static logoff(final Result r) {
        // Terminate the player when socket is closed by the client
        if (clients.containsKey(r.context['handler'])) {
            // Close handlers
            try {
                (clients.get(r.context['handler'])['timer'] as Timer).cancel();
                (clients.get(r.context['handler'])['timer-task'] as TimerTask).cancel();
            } catch (Exception e) { }
            clients.remove(r.context['handler']);
        }
    }


    // Client is connecting with a socket, 'hand-shake'
    @Alert('console shake')
    public static loginConsole(SocketResult r) {
        // Timers control the 'ping/pong'
        def timers = [
                'timer' : new Timer(),
                'timer-task' : {
                    if (r.session?.isOpen()) {
                        r.writeToRemote(['intent': 'ping'])
                    } else {
                        (this as TimerTask).cancel()
                    }
                } as TimerTask ]
        // Start the timers to ping every 25 seconds
        (timers['timer'] as Timer).schedule(timers['timer-task'] as TimerTask, 25000, 25000)
        // Populate 'clients' for fetching timers/handlers while logged in
        clients.put(r.getHandler(), timers);
    }


    // Client is submitting a groovy script to execute
    @Alert('console submit')
    public static submitConsole(SocketResult r) {
        // validate user
        // TODO
        def shell = new GroovyShell();
        Script s;
        // try to compile
        try {
            shell.setProperty('socket', r)
            shell.setProperty('shell',shell)
            s = shell.parse(r.getArg('script'))
        } catch (Exception e) {
            report(["group":"console","status":"error","message":"Could not compile script."])
            return;
        }
        def returned;
        // try to execute
        try { s.run() } catch (Exception e) {
            report(["group":"console","status":"error","message":"Could not execute script.","error":e])
            return;
        }
        // send response
        report([    'message' : 'A script has been executed.',
                    'console-remote' : r.getSession().getRemoteAddress().hostString,
                    'console-local' : r.getSession().getLocalAddress().hostString,
                    'group' : 'console',
                    'status' : 'ok',
                    'script' : r.getArg('script'),
                    'response' : r.context['output']
                ])
    }



}
