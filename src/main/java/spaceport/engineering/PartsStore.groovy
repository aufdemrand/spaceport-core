package spaceport.engineering

import spaceport.Spaceport
import spaceport.bridge.Command
import spaceport.computer.Alerts
import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import spaceport.computer.alerts.AnnotatedAlert
import spaceport.computer.utils.ClassScanner

import java.nio.file.FileSystems
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardWatchEventKinds
import java.nio.file.WatchEvent
import java.nio.file.WatchKey
import java.nio.file.WatchService

class PartsStore {

    //
    // Static
    //

    static def classes = []

    public static initialize() {

        // Initial scan to load parts
        scan()

        // Watch build paths
        for (String path in Spaceport.include_paths) {
            println 'Watching ' + path
            watch((path.endsWith('/') ? path : path + '/'))
        }

        println watching

    }

    // Keep track of all Threads being used to watch directory changes
    public static watching = []

    // Watch
    public static watch(String path) {

        // Watch for filesystem changes and scan() when necessary
        Runnable r = new Runnable() {
            @Override
            void run() {
                Path folder = Paths.get(path);
                WatchService watchService = FileSystems.getDefault().newWatchService();
                folder.register(watchService,
                        StandardWatchEventKinds.ENTRY_CREATE,
                        StandardWatchEventKinds.ENTRY_DELETE,
                        StandardWatchEventKinds.ENTRY_MODIFY);

                boolean valid = true;

                while (valid) {
                    WatchKey watchKey = watchService.take()

                    for (WatchEvent event : watchKey.pollEvents()) {

                        if (StandardWatchEventKinds.ENTRY_CREATE.equals(event.kind())) {
                            String fileName = event.context().toString()
                            println("File Created:" + fileName)
                        }

                        if (StandardWatchEventKinds.ENTRY_DELETE.equals(event.kind())) {
                            String fileName = event.context().toString()
                            println("File deleted:" + fileName)
                        }

                        if (StandardWatchEventKinds.ENTRY_MODIFY.equals(event.kind())) {
                            String fileName = event.context().toString()
                            println("File modified:" + fileName)
                            try { scan() } catch (Exception e) {
                                println('ERROR during file update from WatchService. May need to run PartsStore.scan() again for total initialization.')
                                e.printStackTrace()
                            }
                        }
                    }

                    valid = watchKey.reset();
                }

                if (!valid) println 'Watchkey reset failed.'
            }
        }

        // Watch in new Thread
        Thread t = new Thread(r)
        watching.add(t)
        t.start()
    }

    static boolean hasLoaded = false;

    public static scan() {

        if (hasLoaded) {
            Command.report([
                    'group' : 'Includes',
                    'message' : 'Deinitializing parts.' ])
            Alerts.invoke('on include deinitialize', [:])
            Alerts.invoke('on deinitialize', [:])
            for (Class i in classes)
                ClassScanner.loadedClasses.remove(i)
            classes.clear()
        }

        ClassScanner.classLoader = new GroovyClassLoader()

        for (String path in Spaceport.include_paths) {
            ClassScanner.classLoader.addClasspath(path)
            // Get directory listing
            File[] directoryListing = FileUtils.listFiles(new File(path), null, false);
            // Load all .groovy files
            if (directoryListing != null) {
                for (File child : directoryListing) {
                    if (FilenameUtils.getExtension(child.getName()) == 'groovy') {
                        try {
                            Class c = ClassScanner.classLoader.parseClass(child)
                            classes.add(c)
                            ClassScanner.loadedClasses.add(c)
                            println 'Loading include "' + child + '"'
                        } catch (Exception e) { e.printStackTrace() }
                    }
                }
            }
        }

        Command.report([
                'group'    : 'Includes',
                'message'  : 'Loaded parts.',
                'engineering' : ClassScanner.classLoader.loadedClasses.toArrayString() ])

        hasLoaded = true;

        // Process classes for hooks
        AnnotatedAlert.getStaticHooks()

        Command.report([
                'group' : 'Includes',
                'message' : 'Initializing parts.' ])

        Alerts.invoke('on include initialize', [:])
        Alerts.invoke('on initialize', [:])

    }

}
